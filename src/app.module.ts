import { Module } from '@nestjs/common';
import { ClientModule } from './modules/clients/client.module';
import { TypeOrmModule } from 'src/modules/typeorm.module';
import { OrderModule } from './modules/orders/order.module';

@Module({
  imports: [
    ClientModule,
    TypeOrmModule,
    OrderModule,
  ]
})
export class AppModule { }
