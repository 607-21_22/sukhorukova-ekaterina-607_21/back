import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './order.entity';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { ClientModule } from '../clients/client.module';
import { ProductModule } from '../products/product.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([Order]),
        ClientModule,
        ProductModule
    ],
    controllers: [OrderController],
    providers: [OrderService],
    exports: [OrderService]
})
export class OrderModule { }