import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from "@nestjs/common";
import { OrderService } from "./order.service";
import { CreateOrderDto } from "./dto/create-order.dto";

@Controller('order')
export class OrderController {

    constructor(private readonly OrderService: OrderService) { }

    @Get('/:id')
    async getOrderById(@Param("id",) id: number) {
        return this.OrderService.getOrderById(id)
    }

    @Get('/getall/:id')
    async getAllClientOrders(@Param("id", ParseIntPipe) id: number) {
        return this.OrderService.getAllClientOrders(id)
    }

    @Post('/create')
    async createOrder(@Body() orderData: CreateOrderDto, fio: string) {
        return this.OrderService.createOrder(orderData, fio)
    }

    @Delete('delete/:id')
    async deleteOrder(@Param("id") id: number) {
        return this.OrderService.deleteOrder(id)
    }
}