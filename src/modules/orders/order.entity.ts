import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm'
import { Client } from '../clients/client.entity'
import { Product } from '../products/product.entity'

@Entity('order')
export class Order {

    @PrimaryGeneratedColumn()
    id: number

    @ManyToOne(() => Client, client => client.order)
    client: Client

    @Column({ name: 'order_status', comment: 'Статус заказа', type: 'varchar' })
    status: string

    @ManyToOne(() => Product, product => product.order)
    product: Product

}