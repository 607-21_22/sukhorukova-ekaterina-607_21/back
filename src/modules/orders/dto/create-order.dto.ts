import { IsBoolean, IsNumber, IsString } from "class-validator";

export class CreateOrderDto {

    @IsNumber()
    client: number;

    @IsNumber()
    product: number;

}