import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Order } from "./order.entity"
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateOrderDto } from "./dto/create-order.dto";
import { ClientService } from "../clients/client.service";
import { ProductService } from "../products/product.service";

@Injectable()
export class OrderService {
    constructor(
        @InjectRepository(Order)
        private orderRepository: Repository<Order>,
        private readonly productService: ProductService,
        private readonly cleintService: ClientService
    ) { }

    public async createOrder(orderData: CreateOrderDto, fio: string) {
        const client = await this.cleintService.getClientByFio(fio)
        const product = await this.productService.getProductById(orderData.product)
        const statuses = ['В пути на сорт. центр', 'Доставлен', 'Ожидание оплаты', 'Ждёт отправления', 'Отдан курьеру']
        if (client !== 'Nothing was found') {
            const newOrder = this.orderRepository.create({
                client,
                status: statuses[Math.floor(Math.random() * statuses.length)],
                product,
            })
            return await this.orderRepository.save(newOrder)
        }
    }

    public async getOrderById(orderId: number) {
        const order = await this.orderRepository.findOne({
            relations: {
                client: true,
                product: true
            },
            where: {
                id: orderId
            }
        })
        if (order) return order
        else return "Nothing was found"
    }

    public async getAllClientOrders(clientId: number) {
        const client = await this.cleintService.getClientById(clientId)
        if (client !== "Nothing was found") {
            const orders = await this.orderRepository.find({
                relations: {
                    client: true,
                    product: true
                },
                where: {
                    client
                }
            })
            return orders
        }
        else return "Клиента с таким id нет в базе"
    }

    public async deleteOrder(id: number) {
        const res = await this.orderRepository.delete(id)
        if (res.affected !== 0) return "Deleted"
        else return "Nothing to delete"
    }
}