import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Product } from "./product.entity"
import { InjectRepository } from "@nestjs/typeorm";
import { MoreThan, Repository } from "typeorm";
import { CreateProductDto } from "./dto/create-product.dto";

@Injectable()
export class ProductService {
    constructor(
        @InjectRepository(Product)
        private productRepository: Repository<Product>,
    ) { }

    public async createProduct(dto: CreateProductDto) {
        const canditate = await this.productRepository.findOneBy({
            title: dto.title
        })
        if (canditate) {
            throw new HttpException(
                "Продукт с таким названием уже существует",
                HttpStatus.BAD_REQUEST,
            )
        }
        const newProduct = this.productRepository.create(dto)
        return await this.productRepository.save(newProduct)
    }

    public async deleteProduct(id: number) {
        const res = await this.productRepository.delete(id)
        if (res.affected !== 0) return "Deleted"
        else return "Nothing to delete"
    }

    public async getProductById(productId: number) {
        const product = await this.productRepository.findOneBy({
            id: productId
        })
        return product
    }

    public async getProductByTitle(title: string) {
        const product = await this.productRepository.findOneBy({
            title: title
        })
        if (product) return product
        else return "Nothing was found"
    }

    public async getProductByCount(count: number) {
        const products = await this.productRepository.find({
            where: {
                count: MoreThan(count),
            },
        })
        if (products.length === 0) return "Nothing was found"
        else return products
    }
}