import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from "@nestjs/common";
import { CreateProductDto } from "./dto/create-product.dto";
import { ProductService } from "./product.service";

@Controller('products')
export class ProductController {

    constructor(private readonly productService: ProductService) { }

    @Get('/:id')
    async getProductById(@Param("id",) id: number) {
        return this.productService.getProductById(id)
    }

    @Get('/title/:title')
    async getProductByTitle(@Param("title",) title: string) {
        return this.productService.getProductByTitle(title)
    }

    @Get('/count/:count')
    async getProductByCount(@Param("count",) count: number) {
        return this.productService.getProductByCount(count)
    }

    @Post('/create')
    async createProduct(@Body() productData: CreateProductDto) {
        return this.productService.createProduct(productData)
    }

    @Delete('delete/:id')
    async deleteProduct(@Param("id") id: number) {
        return this.productService.deleteProduct(id)
    }
}