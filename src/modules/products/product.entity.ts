import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm'
import { Order } from '../orders/order.entity'

@Entity('product')
export class Product {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ name: 'title', comment: 'Название', type: 'varchar' })
    title: string

    @Column({ name: 'category', comment: 'Категория товара', type: 'varchar' })
    category: string

    @Column({ name: 'price', comment: 'Цена товара', type: 'varchar' })
    price: number

    @Column({ name: 'count', comment: 'Кол-во на складе', type: 'integer' })
    count: number

    @OneToMany(() => Order, order => order.product)
    order: Order
}