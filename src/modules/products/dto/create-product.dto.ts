import { IsBoolean, IsNumber, IsString } from "class-validator";

export class CreateProductDto {

    @IsString({ message: 'Поле title должно быть строкой' })
    title: string

    @IsString({ message: 'Поле category должно быть строкой' })
    category: string

    @IsNumber()
    price: number

    @IsNumber()
    count: number

}