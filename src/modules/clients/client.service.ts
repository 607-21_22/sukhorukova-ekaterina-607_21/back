import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Client } from "./client.entity"
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateClientDto } from "./dto/create-client.dto";

@Injectable()
export class ClientService {
    constructor(
        @InjectRepository(Client)
        private clientRepository: Repository<Client>,
    ) { }

    public async createClient(dto: CreateClientDto) {
        const canditate = await this.clientRepository.findOneBy({
            FIO: dto.FIO
        })
        if (canditate) {
            throw new HttpException(
                "Клиент с таким именем уже существует ",
                HttpStatus.BAD_REQUEST,
            )
        }
        const newClient = this.clientRepository.create(dto)
        return await this.clientRepository.save(newClient)
    }

    public async deleteClient(id: number) {
        await this.clientRepository.delete(id)
        return { status: HttpStatus.OK }
    }

    public async getClientById(clientId: number) {
        const client = await this.clientRepository.findOneBy({
            id: clientId
        })
        if (client) return client
        else return "Nothing was found"
    }

    public async getClientByFio(fio: string) {
        const client = await this.clientRepository.findOneBy({
            FIO: fio
        })
        if (client) return client
        else return "Nothing was found"
    }
}