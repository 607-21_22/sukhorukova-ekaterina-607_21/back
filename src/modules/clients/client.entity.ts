import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm'
import { Order } from '../orders/order.entity'

@Entity('client')
export class Client {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ name: 'FIO', comment: 'ФИО', type: 'varchar' })
    FIO: string

    @Column({ name: 'phone', comment: 'номер телефона', type: 'varchar' })
    phone: string

    @Column({ name: 'adres', comment: 'Адрес проживания', type: 'varchar' })
    adres: string

    @OneToMany(() => Order, order => order.client)
    order: Order[]
}