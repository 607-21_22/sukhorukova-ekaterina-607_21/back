import { IsBoolean, IsNumber, IsString } from "class-validator";

export class CreateClientDto {

    @IsString({ message: 'Поле FIO должно быть строкой' })
    FIO: string;

    @IsString({ message: 'Поле adres должно быть строкой' })
    adres: string;

    @IsString({ message: 'Поле phone должно быть строкой' })
    phone: string;

}