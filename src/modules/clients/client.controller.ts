import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from "@nestjs/common";
import { ClientService } from "./client.service";
import { CreateClientDto } from "./dto/create-client.dto";

@Controller('clients')
export class ClientController {

    constructor(private readonly taskService: ClientService) { }

    @Get('/:id')
    async getClientById(@Param("id",) id: number) {
        return this.taskService.getClientById(id)
    }

    @Get('/fio/:Fio')
    async getClientByFio(@Param("Fio",) Fio: string) {
        return this.taskService.getClientByFio(Fio)
    }

    @Post('/create')
    async createClient(@Body() taskData: CreateClientDto) {
        return this.taskService.createClient(taskData)
    }

    @Delete('delete/:id')
    async deleteClient(@Param("id") id: number) {
        return this.taskService.deleteClient(id)
    }
}